# Datasets


|               |                       |                              | Low weight variance              |                                |                              | High weight variance             |                                |
|:--------------|:----------------------|:-----------------------------|:---------------------------------|:-------------------------------|:-----------------------------|:---------------------------------|:-------------------------------|
|               |                       | Low insertions and deletions | Regular insertions and deletions | Spiky insertions and deletions | Low insertions and deletions | Regular insertions and deletions | Spiky insertions and deletions |
|               | Low weight change     | [124](#llll)                 |                                  |                                |                              |                                  |                                |
| Single level  | Regular weight change | [12](#lllr)                  | 3                                | 3                              |                              |                                  |                                |
|               | Spiky weight change   | [11](#llls)                  | 3                                | 3                              |                              |                                  |                                |
|               | Low weight change     | 1                            | 2                                | 2                              |                              |                                  |                                |
| 2 or 3 levels | Regular weight change | 1                            | 2                                |                                |                              |                                  |                                |
|               | Spiky weight change   | 2                            | 1                                |                                |                              |                                  |                                |
|               | Low weight change     | 3                            | 1                                |                                |                              |                                  |                                |
| 4+ levels     | Regular weight change | 4                            | 2                                |                                |                              |                                  |                                |
|               | Spiky weight change   | 1                            | 1                                |                                |                              |                                  |                                |



## Low weight variance / Low insertions and deletions / Single level / Low weight change {#llll}

| id                     | leaves | timesteps | levels | weight variance | speed of weight change | insertions and deletions |
|:-----------------------|:-------|:----------|:-------|:----------------|:-----------------------|:-------------------------|
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | low                      |


## Low weight variance / Low insertions and deletions / Single level / Regular weight change {#lllr}

| id                     | leaves | timesteps | levels | weight variance | speed of weight change | insertions and deletions |
|:-----------------------|:-------|:----------|:-------|:----------------|:-----------------------|:-------------------------|
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | regular                  |

## Low weight variance / Low insertions and deletions / Single level / Spiky weight change {#llls}

| id                     | leaves | timesteps | levels | weight variance | speed of weight change | insertions and deletions |
|:-----------------------|:-------|:----------|:-------|:----------------|:-----------------------|:-------------------------|
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |
| f-wb-EN.ATM.CO2E.LF.ZS | 205    | 55        | 1      | low             | low                    | spiky                    |


Figure out how to link to datasets.
