# Runs



|           | Algo 1             | Algo 2   | Algo 3   | Algo 4 | Algo 5 |
|:----------|:-------------------|:---------|:---------|:-------|:-------|
| dataset 1 | [csvs]() [video]() | [.csv]() | [.csv]() |        |        |
| dataset 2 | [csvs]() [video]() | [.csv]() | [.csv]() |        |        |
| dataset 3 |                    |          |          |        |        |
| dataset 4 |                    |          |          |        |        |
| dataset 5 |                    |          |          |        |        |
